<?php

function reverseArray(array $arr, int $start, int $end) {
	while ($start < $end) {
		$temp = $arr[$start];
		$arr[$start] = $arr[$end];
		$arr[$end] = $temp;
		$start += 1;
		$end -= 1;
	}
	return $arr;
}


function rotLeft(array $a, int $d) {
	
	$len = count($a);
	$rotate = $d % $len;
	
	if ($rotate === 0) {
		return $a;
	}
	
	$a = reverseArray($a, 0, $len - 1);
	$a = reverseArray($a, 0, $len - $rotate - 1);
	$a = reverseArray($a, $len - $rotate, $len - 1);
	
	return $a;
}


// test this function
// $roratedArr = rotLeft([1,2,3,4,5], 2);
// echo implode(" ", $roratedArr); // print 3 4 5 1 2 


